/*
  Copyright (c) 2024 Karl Chang
  All rights reserved.

  BSD 3-Clause License
*/

// Define device name
//#define MDNS_NAME "NAc"
#define LED_PIN LED_BUILTIN
#define AC_IR_SEND_PIN D2

#include <ESP8266WiFi.h>
#include <IRremoteESP8266.h>
#include <ir_Mitsubishi.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServer.h>
#include <ESP8266mDNS.h>

IRMitsubishi136 ac(AC_IR_SEND_PIN);
ESP8266WebServer httpServer(80);
ESP8266HTTPUpdateServer httpUpdater;

// eeprom function
extern void eeprom_config_init();
extern char* eeprom_config_ssid_get();
extern char* eeprom_config_password_get();
// Config Web api
extern void ConfigWifiByAP();
// Web page
extern void WebPage_api();

//Config AP web page

// the setup function runs once when you press reset or power the board
void setup() {

  Serial.begin(115200);

  // initialize digital pin LED_PIN as an output.
  pinMode(LED_PIN, OUTPUT);
  ac.begin();

  WiFi.mode(WIFI_STA);

  // Wifi config
  eeprom_config_init();
  if (eeprom_config_ssid_get()[0] == '\0') {
    Serial.println("No config, Enter config wifi mode.");
    ConfigWifiByAP();
  }
  Serial.println("Config restore.");
  Serial.print("Config SSID : ");
  Serial.println(eeprom_config_ssid_get());
  WiFi.begin(eeprom_config_ssid_get(), eeprom_config_password_get());


  int i = 0;
  while (WiFi.status() != WL_CONNECTED) {
    if (i % 10 == 0) {
      Serial.print(".");
    }
    // BLink led for wifi connect
    digitalWrite(LED_PIN, (millis() / 200) % 2);

    if (i > 200) {
      // connect fail in 20sec, open AP mode to config wifi .
      ConfigWifiByAP();
    }
    delay(100);
    i++;
  }

  // wifi connected
  analogWrite(LED_PIN, 252);

  Serial.print("SSID : ");
  Serial.println(WiFi.SSID());
  Serial.print("IP : ");
  Serial.println(WiFi.localIP().toString());
  Serial.print("GW : ");
  Serial.println(WiFi.gatewayIP().toString());
  Serial.print("MAC : ");
  Serial.println(WiFi.macAddress());

#ifdef MDNS_NAME
  String mDnsName = MDNS_NAME;
#else
  String mDnsName = "NAc" + String(ESP.getChipId());
#endif

  if (!MDNS.begin(mDnsName)) {
    Serial.println("Error setting up MDNS responder!");
    while (1) { delay(1000); }
  }
  Serial.print("MDNS : ");
  Serial.println(mDnsName);

  // For web update firmware interface
  httpUpdater.setup(&httpServer);

  // Web page
  httpServer.on("/", WebPage_api);
  httpServer.begin();
}

// the loop function runs over and over again forever
void loop() {
  MDNS.update();

  // web update bin routine
  httpServer.handleClient();  

  if (WiFi.status() != WL_CONNECTED) {
    // BLink for wait connect
    digitalWrite(LED_PIN, (millis() / 200) % 2);
  } else {
    analogWrite(LED_PIN, 252);
  }

  delay(100);
}
