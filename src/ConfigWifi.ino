/*
  Modify from 
  https://github.com/tzapu/WiFiManager
  Copyright (c) 2015 tzapu
*/

// AP Captive Portal to config wifi setting
#include <DNSServer.h>

// Web server
extern ESP8266WebServer httpServer;

// eeprom function
void eeprom_config_save(char* ssid, char* pw);
char* eeprom_config_ssid_get();

// Function
void handleRoot();
void handleWifi();
void handleWifiSave();
void handleNotFound();
boolean captivePortal();
boolean isIp(String str);
String toStringIp(IPAddress ip);

char ConfigSsid[32 + 1] = "";
char ConfigPassword[64 + 1] = "";

/* Soft AP network parameters */
IPAddress apIP(192, 168, 4, 1);
IPAddress netMsk(255, 255, 255, 0);

#ifdef MDNS_NAME
  String softAP_ssid = MDNS_NAME;
#else
  String softAP_ssid = "NAc" + String(ESP.getChipId());
#endif

const char* softAP_password = "";
String myHostname = softAP_ssid;


const char AP_HTTP_HEAD[] PROGMEM = "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"UTF-8\" name=\"viewport\" content=\"width=device-width, initial-scale=1, user-scalable=no\"/><title>{v}</title>";
const char AP_HTTP_STYLE[] PROGMEM = "<style>.c{text-align: center;} div,input{padding:5px;font-size:1em;} input{width:95%;} body{text-align: center;font-family:verdana;} button{border:0;border-radius:0.3rem;background-color:#1fa3ec;color:#fff;line-height:2.4rem;font-size:1.2rem;width:100%;} .q{float: right;width: 64px;text-align: right;} .l{background: url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAALVBMVEX///8EBwfBwsLw8PAzNjaCg4NTVVUjJiZDRUUUFxdiZGSho6OSk5Pg4eFydHTCjaf3AAAAZElEQVQ4je2NSw7AIAhEBamKn97/uMXEGBvozkWb9C2Zx4xzWykBhFAeYp9gkLyZE0zIMno9n4g19hmdY39scwqVkOXaxph0ZCXQcqxSpgQpONa59wkRDOL93eAXvimwlbPbwwVAegLS1HGfZAAAAABJRU5ErkJggg==\") no-repeat left center;background-size: 1em;}</style>";
const char AP_HTTP_SCRIPT[] PROGMEM = "<script>function c(l){document.getElementById('s').value=l.innerText||l.textContent;document.getElementById('p').focus();}</script>";
const char AP_HTTP_HEAD_END[] PROGMEM = "</head><body><div style='text-align:left;display:inline-block;min-width:260px;'>";
const char AP_HTTP_PORTAL_OPTIONS[] PROGMEM = "<form action=\"/wifi\" method=\"get\"><button>Configure WiFi</button></form><br/><form action=\"/0wifi\" method=\"get\"><button>Configure WiFi (No Scan)</button></form><br/><form action=\"/i\" method=\"get\"><button>Info</button></form><br/><form action=\"/r\" method=\"post\"><button>Reset</button></form>";
const char AP_HTTP_ITEM[] PROGMEM = "<div><a href='#p' onclick='c(this)'>{v}</a>&nbsp;<span class='q {i}'>{r}%</span></div>";
const char AP_HTTP_FORM_START[] PROGMEM = "<form method='get' action='wifisave'><input id='s' name='s' length=32 placeholder='SSID'><br/><input id='p' name='p' length=64 type='password' placeholder='password'><br/>";
const char AP_HTTP_FORM_PARAM[] PROGMEM = "<br/><input id='{i}' name='{n}' maxlength={l} placeholder='{p}' value='{v}' {c}>";
const char AP_HTTP_FORM_END[] PROGMEM = "<br/><button type='submit'>save</button></form>";
const char AP_HTTP_SCAN_LINK[] PROGMEM = "<br/><div class=\"c\"><a href=\"/wifi\">Scan</a></div>";
const char AP_HTTP_SAVED[] PROGMEM = "<div>Credentials Saved<br />Trying to connect ESP to network.<br />If it fails reconnect to AP to try again</div>";
const char AP_HTTP_END[] PROGMEM = "</div></body></html>";


void ConfigWifiByAP() {
  // DNS server
  const byte DNS_PORT = 53;
  DNSServer dnsServer;

  WiFi.disconnect();

  WiFi.softAPConfig(apIP, apIP, netMsk);
  WiFi.softAP(softAP_ssid, softAP_password);
  delay(500);

  /* Setup the DNS server redirecting all the domains to the apIP */
  dnsServer.setErrorReplyCode(DNSReplyCode::NoError);
  dnsServer.start(DNS_PORT, "*", apIP);

  /* Setup web pages: root, wifi config pages, SO captive portal detectors and not found. */
  httpServer.on("/", handleRoot);
  httpServer.on("/wifi", handleWifi);
  httpServer.on("/wifisave", handleWifiSave);
  httpServer.on("/generate_204", handleRoot);  //Android captive portal. Maybe not needed. Might be handled by notFound handler.
  httpServer.on("/fwlink", handleRoot);        //Microsoft captive portal. Maybe not needed. Might be handled by notFound handler.
  httpServer.onNotFound(handleNotFound);

#ifdef BUILD_NAC_CONTROL
  httpServer.on("/", WebPage_api);
#endif
  httpServer.begin();  // Web server start

  unsigned long startMillis = millis();

  while (true) {
    if ((WiFi.status() != WL_CONNECTED) && (strlen(ConfigSsid) > 0)) {
      WiFi.disconnect();
      WiFi.begin(ConfigSsid, ConfigPassword);
      int connRes = WiFi.waitForConnectResult();
      Serial.print("connRes: ");
      Serial.println(connRes);

      if (connRes == WL_CONNECTED) {
        eeprom_config_save(ConfigSsid, ConfigPassword);
        Serial.println("Wifi config save.");
        Serial.print("Config SSID : ");
        Serial.println(ConfigSsid);
        Serial.print("Config password : ");
        Serial.println(ConfigPassword);

        Serial.println("Reboot after 2 second.");
        delay(2000);
        ESP.restart();
        delay(2000);
      } else
        ConfigSsid[0] = '\0';  //connect fail . restart web config
    }

    if (millis() - startMillis > (1000 * 60 * 5))  // 5mis reboot, try connect wifi by config again.
    {
      Serial.println("every 5mis reboot, try connect wifi by config again.\nReboot after 2 second.");
      delay(2000);
      ESP.restart();
      delay(2000);
    }

#ifdef LED_PIN
    //Blink
    digitalWrite(LED_PIN, (millis() / 2000) % 2);
#endif
#ifdef BUILD_NPORT_POWER_CONTROL
    button.tick();
#endif
    //DNS
    dnsServer.processNextRequest();
    //HTTP
    httpServer.handleClient();
    delay(100);
  }
}

/** Handle root or redirect to captive portal */
void handleRoot() {
  if (captivePortal()) {  // If caprive portal redirect instead of displaying the page.
    return;
  }

  String page = FPSTR(AP_HTTP_HEAD);
  page.replace("{v}", "Options");
  page += FPSTR(AP_HTTP_SCRIPT);
  page += FPSTR(AP_HTTP_STYLE);
  page += FPSTR(AP_HTTP_HEAD_END);
  page += String(F("<h1>"));
  page += softAP_ssid;
  page += String(F("</h1>"));
  page += String(F("<h3>WiFiManager</h3>"));
  page += String(F("<form action=\"/wifi\" method=\"get\"><button>Configure WiFi</button></form><br/>"));

  /// infomation
  page += F("<dl>");
  page += F("<dt>Current Wifi STA SSID setting</dt><dd>");
  page += eeprom_config_ssid_get();
  page += F("<dt>Project Name</dt><dd>");
  page += softAP_ssid;
  page += F("<dt>Chip ID</dt><dd>");
  page += ESP.getChipId();
  page += F("</dd>");
  page += F("<dt>Flash Chip ID</dt><dd>");
  page += ESP.getFlashChipId();
  page += F("</dd>");
  page += F("<dt>IDE Flash Size</dt><dd>");
  page += ESP.getFlashChipSize();
  page += F(" bytes</dd>");
  page += F("<dt>Real Flash Size</dt><dd>");
  page += ESP.getFlashChipRealSize();
  page += F(" bytes</dd>");
  page += F("<dt>Soft AP IP</dt><dd>");
  page += WiFi.softAPIP().toString();
  page += F("</dd>");
  page += F("<dt>Soft AP MAC</dt><dd>");
  page += WiFi.softAPmacAddress();
  page += F("</dd>");
  page += F("<dt>Station MAC</dt><dd>");
  page += WiFi.macAddress();
  page += F("</dd>");
  page += F("</dl>");

  ///

  page += FPSTR(AP_HTTP_END);
  httpServer.sendHeader("Content-Length", String(page.length()));
  httpServer.send(200, "text/html", page);
  httpServer.client().stop();  // Stop is needed because we sent no content length
}

/** Redirect to captive portal if we got a request for another domain. Return true in that case so the page handler do not try to handle the request again. */
boolean captivePortal() {
  if (!isIp(httpServer.hostHeader()) && httpServer.hostHeader() != (String(myHostname) + ".local")) {
    Serial.println("Request redirected to captive portal");
    httpServer.sendHeader("Location", String("http://") + toStringIp(httpServer.client().localIP()), true);
    httpServer.send(302, "text/plain", "");  // Empty content inhibits Content-length header so we have to close the socket ourselves.
    httpServer.client().stop();              // Stop is needed because we sent no content length
    return true;
  }
  return false;
}


const char* _customHeadElement = "";
boolean _removeDuplicateAPs = true;
int _minimumQuality = -1;

#define DEBUG_WM Serial.println

int getRSSIasQuality(int RSSI) {
  int quality = 0;

  if (RSSI <= -100) {
    quality = 0;
  } else if (RSSI >= -50) {
    quality = 100;
  } else {
    quality = 2 * (RSSI + 100);
  }
  return quality;
}

/** Wifi config page handler */
void handleWifi() {
  boolean scan = true;

  String page = FPSTR(AP_HTTP_HEAD);
  page.replace("{v}", "Config ESP");
  page += FPSTR(AP_HTTP_SCRIPT);
  page += FPSTR(AP_HTTP_STYLE);
  page += _customHeadElement;
  page += FPSTR(AP_HTTP_HEAD_END);

  if (scan) {
    int n = WiFi.scanNetworks();
    DEBUG_WM(F("Scan done"));
    if (n == 0) {
      DEBUG_WM(F("No networks found"));
      page += F("No networks found. Refresh to scan again.");
    } else {

      //sort networks
      int indices[n];
      for (int i = 0; i < n; i++) {
        indices[i] = i;
      }

      // RSSI SORT

      // old sort
      for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
          if (WiFi.RSSI(indices[j]) > WiFi.RSSI(indices[i])) {
            std::swap(indices[i], indices[j]);
          }
        }
      }

      // remove duplicates ( must be RSSI sorted )
      if (_removeDuplicateAPs) {
        String cssid;
        for (int i = 0; i < n; i++) {
          if (indices[i] == -1) continue;
          cssid = WiFi.SSID(indices[i]);
          for (int j = i + 1; j < n; j++) {
            if (cssid == WiFi.SSID(indices[j])) {
              DEBUG_WM("DUP AP: " + WiFi.SSID(indices[j]));
              indices[j] = -1;  // set dup aps to index -1
            }
          }
        }
      }

      //display networks in page
      for (int i = 0; i < n; i++) {
        if (indices[i] == -1) continue;  // skip dups
        DEBUG_WM(WiFi.SSID(indices[i]));
        DEBUG_WM(WiFi.RSSI(indices[i]));
        int quality = getRSSIasQuality(WiFi.RSSI(indices[i]));

        if (_minimumQuality == -1 || _minimumQuality < quality) {
          String item = FPSTR(AP_HTTP_ITEM);
          String rssiQ;
          rssiQ += quality;
          item.replace("{v}", WiFi.SSID(indices[i]));
          item.replace("{r}", rssiQ);
          if (WiFi.encryptionType(indices[i]) != ENC_TYPE_NONE) {
            item.replace("{i}", "l");
          } else {
            item.replace("{i}", "");
          }
          //DEBUG_WM(item);
          page += item;
          delay(0);
        } else {
          DEBUG_WM(F("Skipping due to quality"));
        }
      }
      page += "<br/>";
    }
  }


  page += FPSTR(AP_HTTP_FORM_START);
  page += FPSTR(AP_HTTP_FORM_END);
  page += FPSTR(AP_HTTP_SCAN_LINK);
  page += FPSTR(AP_HTTP_END);

  httpServer.sendHeader("Content-Length", String(page.length()));
  httpServer.send(200, "text/html", page);

  DEBUG_WM(F("Sent config page"));
  httpServer.client().stop();  // Stop is needed because we sent no content length
  return;
}


/** Handle the WLAN save form and redirect to WLAN config page again */
void handleWifiSave() {
  Serial.println("wifi save");
  httpServer.arg("s").toCharArray(ConfigSsid, sizeof(ConfigSsid) - 1);
  httpServer.arg("p").toCharArray(ConfigPassword, sizeof(ConfigPassword) - 1);
  httpServer.sendHeader("Location", "wifi", true);
  httpServer.sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  httpServer.sendHeader("Pragma", "no-cache");
  httpServer.sendHeader("Expires", "-1");
  httpServer.send(302, "text/plain", "");  // Empty content inhibits Content-length header so we have to close the socket ourselves.
  httpServer.client().stop();              // Stop is needed because we sent no content length
  //saveCredentials();
}

void handleNotFound() {
  if (captivePortal()) {  // If caprive portal redirect instead of displaying the error page.
    return;
  }
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += httpServer.uri();
  message += "\nMethod: ";
  message += (httpServer.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += httpServer.args();
  message += "\n";

  for (uint8_t i = 0; i < httpServer.args(); i++) {
    message += " " + httpServer.argName(i) + ": " + httpServer.arg(i) + "\n";
  }
  httpServer.sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  httpServer.sendHeader("Pragma", "no-cache");
  httpServer.sendHeader("Expires", "-1");
  httpServer.send(404, "text/plain", message);
}

/** Is this an IP? */
boolean isIp(String str) {
  for (unsigned int i = 0; i < str.length(); i++) {
    int c = str.charAt(i);
    if (c != '.' && (c < '0' || c > '9')) {
      return false;
    }
  }
  return true;
}

/** IP to String? */
String toStringIp(IPAddress ip) {
  String res = "";
  for (int i = 0; i < 3; i++) {
    res += String((ip >> (8 * i)) & 0xFF) + ".";
  }
  res += String(((ip >> 8 * 3)) & 0xFF);
  return res;
}
