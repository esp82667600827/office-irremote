/*
  Copyright (c) 2024 Karl Chang
  All rights reserved.

  BSD 3-Clause License
*/

#include <EEPROM.h>
// EEPROM partition
// 32 Bytes - SSID
// 64 Bytes - Password

#define EEPROM_CONFIG_SSID_START  0
#define EEPROM_CONFIG_SSID_LEN    32

#define EEPROM_CONFIG_PASSWORD_START  32
#define EEPROM_CONFIG_PASSWORD_LEN    64

#define EEPROM_CONFIG_SIZE  128

char eeprom_config_ssid[EEPROM_CONFIG_SSID_LEN+1];
char eeprom_config_password[EEPROM_CONFIG_PASSWORD_LEN+1];

void eeprom_config_init()
{
  int i,index;

  eeprom_config_ssid[0]='\0';
  eeprom_config_password[0]='\0';
  EEPROM.begin(EEPROM_CONFIG_SIZE);
  if(EEPROM.read(0)==0xff)
  {
    return;
  }

  index = 0;
  eeprom_config_ssid[EEPROM_CONFIG_SSID_LEN]='\0';
  for (i=EEPROM_CONFIG_SSID_START; i<EEPROM_CONFIG_SSID_LEN+EEPROM_CONFIG_SSID_START; i++)
  {
    eeprom_config_ssid[index] = EEPROM.read(i);
    if(eeprom_config_ssid[index] == '\0')
      break;
    index++;
  }

  index = 0;
  eeprom_config_password[EEPROM_CONFIG_PASSWORD_LEN]='\0';
  for (i=EEPROM_CONFIG_PASSWORD_START; i<EEPROM_CONFIG_PASSWORD_LEN+EEPROM_CONFIG_PASSWORD_START; i++)
  {
  
    eeprom_config_password[index] = EEPROM.read(i);
    if(eeprom_config_password[index] == '\0')
      break;
    index++;
  }
}
char* eeprom_config_ssid_get()
{
  return eeprom_config_ssid;
}

char* eeprom_config_password_get()
{
  return eeprom_config_password;
}

void eeprom_config_save(char* ssid,char* pw)
{
  int i,index;
  strcpy(eeprom_config_ssid,ssid);
  strcpy(eeprom_config_password,pw);
  index = 0;
  for (i=EEPROM_CONFIG_SSID_START; i<EEPROM_CONFIG_SSID_LEN+EEPROM_CONFIG_SSID_START; i++)
  {
    EEPROM.write(i,eeprom_config_ssid[index]);
    if(eeprom_config_ssid[index] == '\0')
      break;
    index++;
  }

  index = 0;
  for (i=EEPROM_CONFIG_PASSWORD_START; i<EEPROM_CONFIG_PASSWORD_LEN+EEPROM_CONFIG_PASSWORD_START; i++)
  {
    EEPROM.write(i,eeprom_config_password[index]);
    if(eeprom_config_password[index] == '\0')
      break;
    index++;
  }

  EEPROM.commit();
}

void eeprom_config_clear()
{
  EEPROM.write(0,0xff);
  EEPROM.commit();
}
