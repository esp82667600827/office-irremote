/*
  Copyright (c) 2024 Karl Chang
  All rights reserved.

  BSD 3-Clause License
*/

extern ESP8266WebServer httpServer;
extern IRMitsubishi136 ac;

void WebPage_api() {
  String path = httpServer.uri();
  String message;

  static int temp = 27;
  static int speed = 0;

  if (httpServer.args() < 2) {
    const char* html_content =
      "<!DOCTYPE html>"
      "<html lang=\"en\">"
      "<head>"
      "    <meta charset=\"UTF-8\">"
      "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">"
      "    <title>AC control</title>"
      "    <style>"
      "        body { font-family: Arial, sans-serif; margin: 20px; }"
      "        .control { margin-bottom: 20px; }"
      "        .temperature-control { display: flex; align-items: center; }"
      "        .temperature-control button { padding: 5px 10px; margin: 0 5px; }"
      "        #temp-value { margin: 0 10px; font-size: 1.2em; }"
      "    </style>"
      "</head>"
      "<body>"
      "    <h1>{{NAC_NAME}} control</h1>"
      "    <div class=\"control\">"
      "        <label for=\"wind-speed\">Fan:</label>"
      "        <select id=\"wind-speed\">"
      "            <option value=\"0\">Min</option>"
      "            <option value=\"1\">Low</option>"
      "            <option value=\"2\">Med</option>"
      "            <option value=\"3\">Max</option>"
      "            <option value=\"4\">Off</option>"
      "        </select>"
      "    </div>"
      "    <div class=\"control temperature-control\">"
      "        <button onclick=\"decTemp()\">-</button>"
      "        <span id=\"temp-value\">25</span> °C"
      "        <button onclick=\"incTemp()\">+</button>"
      "    </div>"
      "    <script>"
      "        var apiTmo,temp = {{AC_TEMP}},wsDefault = {{AC_SPEED}};"
      "document.getElementById('wind-speed').selectedIndex = wsDefault;"
      "document.getElementById('temp-value').textContent = temp;"
      "        function updTemp() {"
      "            document.getElementById('temp-value').textContent = temp;"
      "            rstApiTmo();"
      "        }"
      "        function decTemp() {"
      "            if (temp > 17) { temp--; updTemp(); }"
      "        }"
      "        function incTemp() {"
      "            if (temp < 30) { temp++; updTemp(); }"
      "        }"
      "        function rstApiTmo() {"
      "            clearTimeout(apiTmo);"
      "            apiTmo = setTimeout(sndApiReq, 1000);"
      "        }"
      "        function sndApiReq() {"
      "            var ws = document.getElementById('wind-speed').value;"
      "            fetch('/?s=' + ws + ';t=' + temp);"
      "        }"
      "        document.getElementById('wind-speed').addEventListener('change', sndApiReq);"
      "    </script>"
      "</body>"
      "</html>";
    message = html_content;
#ifdef MDNS_NAME
    message.replace("{{NAC_NAME}}", MDNS_NAME );
#else
    message.replace("{{NAC_NAME}}", "NAc" + String(ESP.getChipId()));
#endif
    message.replace("{{AC_TEMP}}", String(temp));
    message.replace("{{AC_SPEED}}", String(speed));
    httpServer.sendHeader("Content-Length", String(message.length()));
    httpServer.send(200, "text/html", message);
    httpServer.client().stop();  // Stop is needed because we sent no content length
  } else {
    String speed_str = httpServer.arg("s");
    String temp_str = httpServer.arg("t");
    temp = temp_str.toInt();
    speed = speed_str.toInt();
    digitalWrite(LED_PIN, LOW);
    if (speed == 4) {
      ac.off();
    } else {
      ac.on();
      ac.setFan(speed);
      ac.setMode(kMitsubishi136Cool);
      ac.setTemp(temp);
    }
    ac.send();
    httpServer.send(200, "text/plain", message);
    httpServer.client().stop();
  }
}