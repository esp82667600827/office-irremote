# office-irremote

## Description
This is a simple air conditioner infrared remote control that allows you to control the Mitsubishi Electric air conditioner settings via a web page.  
The model number of the original remote control is PAR-FL32MA

## Function
- On startup, it will attempt to connect to the AP, with the LED flashing quickly.
- If it doesn't connect within 20 seconds, it will activate the AP function, with the LED flashing slowly. Please connect to this AP network, and it will bring up the Captive portal web page for setting up the connection.
- To reconfigure the AP, ensure the device can't connect to the AP, and after 20 seconds, it will enter the Captive portal web page for setting up the connection.
- After a successful startup, the LED will stay on.
- If you have set MDNS_NAME, you can access the web control for the air conditioner using MDNS_NAME.local, or the SSID will be MDNS_NAME to enter the Captive portal web page for setting up the connection.
- If MDNS_NAME is not set, it will use "NAc" + Chip ID for identification.

## Hardware
You only need to connect the positive terminal of your Infrared Emitting Diodes(IREDs) to the AC_IR_SEND_PIN (currently D2) and the negative terminal to the ground.  
It is recommended to use a transistor to drive the IREDs for better performance.

## Welcome to Collaborate
- Perhaps we can enhance the Captive Portal webpage to not only control the AC but also provide WiFi settings options, allowing users without an AP to control AC through the Captive Portal webpage.
- Any collaboration for optimization is highly welcome.

## License
BSD 3-Clause License

## Third-Party Licenses

### MIT License - WiFiManager

This project includes code from the WiFiManager library, which is licensed under the MIT License.  
The original code can be found at [https://github.com/tzapu/WiFiManager](https://github.com/tzapu/WiFiManager).
